package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRs extends AbstractResultRs {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListRs(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

    public TaskListRs(@NotNull final Throwable err) {
        super(err);
    }

}