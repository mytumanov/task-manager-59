package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByIndexRq extends AbstractUserRq {

    @Nullable
    private Integer index;

    public TaskShowByIndexRq(@Nullable final String token, @Nullable final Integer index) {
        super(token);
        this.index = index;
    }

}