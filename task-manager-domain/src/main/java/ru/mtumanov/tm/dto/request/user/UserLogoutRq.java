package ru.mtumanov.tm.dto.request.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@NoArgsConstructor
public final class UserLogoutRq extends AbstractUserRq {

    public UserLogoutRq(@Nullable final String token) {
        super(token);
    }

}
