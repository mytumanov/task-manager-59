package ru.mtumanov.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @Nullable
    @Column(nullable = false, name = "user_id")
    protected String userId;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AbstractUserOwnedModelDTO)) {
            return false;
        }
        AbstractUserOwnedModelDTO abstractUserOwnedModel = (AbstractUserOwnedModelDTO) o;
        return Objects.equals(userId, abstractUserOwnedModel.userId)
                && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(userId);
    }

}
