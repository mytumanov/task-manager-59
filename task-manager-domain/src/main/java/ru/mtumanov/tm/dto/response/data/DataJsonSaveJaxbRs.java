package ru.mtumanov.tm.dto.response.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@NoArgsConstructor
public final class DataJsonSaveJaxbRs extends AbstractResultRs {

    public DataJsonSaveJaxbRs(@NotNull final Throwable err) {
        super(err);
    }

}
