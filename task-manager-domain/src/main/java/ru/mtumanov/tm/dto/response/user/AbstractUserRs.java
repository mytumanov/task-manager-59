package ru.mtumanov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRs extends AbstractResultRs {

    @Nullable
    private UserDTO user;

    protected AbstractUserRs(@Nullable final UserDTO user) {
        this.user = user;
    }

    protected AbstractUserRs(@NotNull final Throwable err) {
        super(err);
    }

}