package ru.mtumanov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.IListener;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    @Nullable
    public abstract Role[] getRoles();

    @Override
    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty())
            result += name + " : ";
        if (argument != null && !argument.isEmpty())
            result += argument + " : ";
        if (!description.isEmpty())
            result += description;
        return result;
    }

}
