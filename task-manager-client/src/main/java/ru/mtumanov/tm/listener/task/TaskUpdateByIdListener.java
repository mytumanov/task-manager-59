package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskUpdateByIdRq;
import ru.mtumanov.tm.dto.response.task.TaskUpdateByIdRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRq request = new TaskUpdateByIdRq(getToken(), id, name, description);
        @NotNull final TaskUpdateByIdRs response = getTaskEndpoint().taskUpdateById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
