package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.dto.IDtoTaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class TaskDtoService extends AbstractDtoUserOwnedService<TaskDTO, IDtoTaskRepository> implements IDtoTaskService {

    @Override
    @NotNull
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            return Collections.emptyList();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws AbstractException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);

        repository.add(task);
        return task;
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull final TaskDTO task = repository.findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO changeTaskStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        @NotNull final TaskDTO task = repository.findOneById(userId, id);
        task.setStatus(status);
        repository.update(task);
        return task;
    }

}
