package ru.mtumanov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoSessionRepository;
import ru.mtumanov.tm.api.service.dto.IDtoSessionService;
import ru.mtumanov.tm.dto.model.SessionDTO;

@Service
public class SessionDtoService extends AbstractDtoUserOwnedService<SessionDTO, IDtoSessionRepository> implements IDtoSessionService {

}
