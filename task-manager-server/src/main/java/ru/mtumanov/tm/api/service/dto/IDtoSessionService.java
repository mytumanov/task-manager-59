package ru.mtumanov.tm.api.service.dto;

import ru.mtumanov.tm.dto.model.SessionDTO;

public interface IDtoSessionService extends IDtoUserOwnedService<SessionDTO> {

}
