package ru.mtumanov.tm.api.repository.model;

import ru.mtumanov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
