package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.request.user.UserLogoutRq;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.dto.response.user.UserLogoutRs;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.mtumanov.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @Override
    @NotNull
    @WebMethod
    public UserLoginRs login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRq request
    ) {
        try {
            @NotNull final String token = getServiceLocator().getAuthService().login(request.getLogin(), request.getPassword());
            return new UserLoginRs(token);
        } catch (@NotNull final Exception e) {
            return new UserLoginRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public UserLogoutRs logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            getServiceLocator().getAuthService().logout(session);
            return new UserLogoutRs();
        } catch (@NotNull final Exception e) {
            return new UserLogoutRs(e);
        }
    }

}
