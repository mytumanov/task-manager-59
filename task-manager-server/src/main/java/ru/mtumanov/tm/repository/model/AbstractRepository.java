package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.model.IRepository;
import ru.mtumanov.tm.comparator.CreatedComparator;
import ru.mtumanov.tm.comparator.NameComparator;
import ru.mtumanov.tm.comparator.StatusComparator;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Nullable
    protected String getComporator(@NotNull final Comparator comparator) {
        if (comparator instanceof CreatedComparator)
            return "created";
        else if (comparator instanceof NameComparator)
            return "name";
        else if (comparator instanceof StatusComparator)
            return "status";
        else
            return null;
    }

    @Override
    public void add(@NotNull final M entity) throws AbstractException {
        entityManager.persist(entity);
    }

    @Override
    @NotNull
    public M update(@NotNull final M entity) throws AbstractException {
        return entityManager.merge(entity);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
